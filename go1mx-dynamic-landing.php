<?php

/*
Plugin Name: Go1 Dynamic Landing
Plugin URI: go1.mx
Description: Landings con productos dinamicos
Version: 0.5
Author: Ruben Poot
Author URI: go1.mx
*/

require_once 'hooks.php';
include_once __DIR__ . '/acf-home-landing/init.php';    
//require_once __DIR__ . '/acf-home-landing/inc/querys.php';    

add_action( 'elementor/widgets/widgets_registered', function() {

	require_once __DIR__ . '/custom_components/caja_busqueda_dinamica.php';
	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \WPC\CajaBusquedaDinamica() );

  require_once __DIR__ . '/custom_components/hoteles_dinamicos.php';
	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \WPC\HotelesDinamicos() );
  //ShowLandingList
  require_once __DIR__ . '/custom_components/show_landing_list.php';
	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \WPC\ShowLandingList() );

});


function go1mx_dynamic_landing_custom_post()
{
  register_post_type("dynamic-landing", [
    "labels" => [
      "name" => __("Landings dinámicas"),
      "singular_name" => __("Landing dinámica"),
    ],
    "public" => true,
    "has_archive" => false,
    "show_in_rest" => true,
    "rewrite" => ["slug" => "ofertas"],
    "supports" => [
      "title",
      "thumbnail"
    ],
    "menu_icon" => "dashicons-layout",
    "hierarchical" => true,
  ]);

  register_post_type("dynamic-home", [
    "labels" => [
      "name" => __("Homes dinámicos"),
      "singular_name" => __("Home dinámico"),
    ],
    "public" => true,
    "has_archive" => false,
    "show_in_rest" => true,
    "rewrite" => ["slug" => "homes"],
    "supports" => [
      "title",
      "thumbnail"
    ],
    "menu_icon" => "dashicons-welcome-view-site",
    "hierarchical" => true,
  ]);
}

function my_admin_enqueue_scripts()
{
  wp_enqueue_script(
    "my-admin-js",
    plugin_dir_url( __FILE__ ) ."js/admin.js",
    [],
    "1.0.6",
    true
  );
}

//cargar valores previamente selecionados
/*function hoteles_load_field($field)
{
  global $post;

  $hoteles = get_post_meta($post->ID, "configuraciones_hotel_hoteles", true);
  if (is_array($hoteles)) {
    $field["choices"] = [];
    foreach ($hoteles as $hotel) {
      $info = json_decode($hotel);
      $field["choices"][$hotel] = $info->name;
    }
  }
  return $field;
}*/

//codigo
//nuevo codigo 20 12 2023
function hoteles_load_field($field)
{
global $post;
$hoteles=array();
$loadcsv =get_field('configuraciones_hotel_cargar_hoteles_dede_archivo_');
$opcionloader=($loadcsv==null) ? "0" : $loadcsv['value'];
$hoteles = get_post_meta($post->ID, "configuraciones_hotel_hoteles", true);

if($opcionloader=="0"){
	$field['ajax']=1;
	?>
				<script type="text/javascript">
				window.onload = function(){
					ocultarfileloader();
				}
				</script>
	<?php
	$field["choices"] = [];

}

if($opcionloader=="2"){
 // var_dump($hoteles);

   if($hoteles ==""){
				$field['ajax']=0;
				$fiid='171';
				$field["choices"] = array();
				$namefike=apply_filters('Myfiltergetidfilecsv',$fiid);
				 if($namefike['file_path']!=''){
						$hoteles =dcms_show_csv_data($namefike['file_path']);
					}

				}else{
				if (count($hoteles) > 2) {
					$field['ajax']=0;
					$hot = json_encode(get_post_meta($post->ID, "configuraciones_hotel_hoteles", true));
          $hoteles=json_decode($hot);
          $field["choices"] = [];
			}
		}

}


if($hoteles==""){
}else{
	if (is_array($hoteles)) {
		foreach ($hoteles as $hotel) {
      $info = json_decode($hotel);//var_dump($hotel);
			if($opcionloader=="0"){

			$field["choices"][$hotel] = $info->name;
		  }
			if($opcionloader=="2"){
				$field["choices"][$hotel]=$info->code;
			}
    }
	if($opcionloader=="2"){

		?>
				<script type="text/javascript">
				window.onload = function(){
					  detecta();
						ocukyatsaelechotel();  // activar
				}
				</script>
		<?php
		 update_post_meta( $post->ID, "configuraciones_hotel_hoteles",  $hoteles);
		 $fixed = get_post_meta( $post->ID, "configuraciones_hotel_hoteles", true );

	 }


}

}

?>
<script type="text/javascript">
jQuery(function(){
jQuery('input[type="radio"]').on('click', this, function(event){
 if(jQuery(this).val() == '2') {
	 <?php
	  $field["ajax"]=0;//activar
	  ?>

	mostrarfileloader();
	detecta();
  ocukyatsaelechotel(); // activae


 }
 if(jQuery(this).val() == '0') {
 <?php
	$field["ajax"]=1;  //activar
//$field["choices"]="";
//$hoteles="";
	?>
	ocultarfileloader();
	deseleccionartodohotel();
	mostarselecthotel();
 }
});
});
//-------------------------------------------
//funciones si  opcion es Manual
function ocultarfileloader() {
const el = document.querySelectorAll("div.acf-field-file");
      el[0].style.display="none";
}
function mostarselecthotel() {
const el = document.querySelectorAll("div.acf-field-6322b6a7666bd");
      el[0].style.display="block";
}
//funciones si opcion es Csv
function detecta(){
	//let totalloadhotel=0;
 jQuery('#acf-field_6322b65d666bc-field_6322b6a7666bd option').each(function() {
					jQuery(this). prop("selected", true);
					//totalloadhotel++;
 });
}

//funcion para deaactivar selected
function deseleccionartodohotel(){
 jQuery('#acf-field_6322b65d666bc-field_6322b6a7666bd option').each(function() {
					jQuery(this). prop("selected", false);
 });
}

function mostrarfileloader() {
const el = document.querySelectorAll("div.acf-field-file");
		 el[0].style.display="block";
}
function ocukyatsaelechotel() {
const el = document.querySelectorAll("div.acf-field-6322b6a7666bd");
		 el[0].style.display="none";
}
//-------------------------------------------------------------------
</script>

<?php
  return $field;
}


function acfobtenreridarchivo($field){
	global $post;
  $prueba = get_post_meta($post->ID, "configuraciones_hotel_importar_hoteles_de_csv", true);
  $field['value']=(string)$prueba;
	return $field;
}

function myget_fileid($myfeleidok){
	if(acf_get_field('importar_hoteles_de_csv')==false){
			$myfeleidok="";
	}else{

  	$uri =acf_get_field('importar_hoteles_de_csv');
  	$myfeleidokatach=get_file_loadercsv($uri['value']);
  	$myfeleidok = $myfeleidokatach;
	}

	return $myfeleidok;
}

function get_file_loadercsv($filep){
  $url="";
  if(!empty($filep)){
      $filepath=parse_url(wp_get_attachment_url($filep));//get_attachment_link
      $filetitle = '' .rawurlencode( basename( $filepath[ 'path' ] ) ); //nombre del archivo csv
      $url = dirname( $filepath [ 'path' ] );
      $posx1=strlen($url);
      $posx=strpos($url, '/uploads/')+strlen('/uploads');
      $url = substr($url,$posx,$posx1-$posx);
      $url =$url."/".$filetitle;
  }
  $dataFile=array(
    "file_path"=>"$url"
  
  );
  return $dataFile;
  }

function hotelesload_csv($field){
	global $post;

$loadcsv =get_post_meta($post->ID, "configuraciones_hotel_cargar_hoteles_dede_archivo_", true);
$field['value']=$loadcsv;
return $field;
}

function dcms_show_csv_data($namefike=''){
  global $wp_filesystem;
	$upload_path =$wp_filesystem->wp_content_dir();
  $data2 = array();
  $file_path = $upload_path .'/uploads'. $namefike;//'/2023/11/dayosh.csv'; //
  if (file_exists($file_path)) {
              $f = fopen($file_path, "r");
              $i=0;
              while ( $line = fgetcsv($f) ) {
                $nread=trim($line[0]," \t\n\r\0\x0B\xc2\xa0");
                if($nread!=''){
                $data=[
                    "code"=>"$nread"

                ];
                $data2[$i]=json_encode($data);
                $i++;
              }
              }

              fclose($f);
          }

return $data2;
}
//fin de nuevo codigo 20 12 2023

//change image banner 2024
function getUrlImagebanner($namefike,$post_id,$idimage){
  $filename=preg_replace( '/\.[^.]+$/', '', basename( $namefike ) ); //"economico"
  $url = dirname($namefike);//http://localhost/work/ptdealsprueba/wp-content/uploads/2023/08

  $file_path=$url ."/".$filename .".jpg"; //http://localhost/work/ptdealsprueba/wp-content/uploads/2023/08/economico.jpg   
  $attachment_id="";

    if ( has_post_thumbnail() ) { // check if the post has a featured image
          
          $attachment_id_old = get_post_thumbnail_id(); // get the ID of the post thumbnail image
          $imgurl = wp_get_attachment_image_url( $attachment_id_old, '' );
          
              if($file_path ==$imgurl){
              }else{
                $attachment_id=$idimage;
                if ( ! is_wp_error( $attachment_id ) ) {
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                update_attached_file( $post_id, $file_path );
                update_post_meta( $post_id, '_thumbnail_id', $attachment_id );
                $x = get_post_meta($post_id, "_thumbnail_id", true);
              }
      

              }

    }

  }

function getBannerCount($field)
{
  global $post;
  $rowsbanner=0;
  $contadesde =acf_get_field('count_desde');
  //var_dump($contadesde[0]);
  $rowsbanner = get_post_meta($post->ID, "campaign_banner", true);
  //print_r("asasasasasasasasasasasa ".$rowsbanner);
  if($rowsbanner > 0 &&  get_post_type()=='dynamic-home'){
       ?>
       
       <script type="text/javascript">
				window.onload = function(){
          disabledrowbanner();
        }
				</script>

       <?php 
  }
 ?>
<script type="text/javascript">

function disabledrowbanner(){

const apgbannercounterdesde=document.querySelectorAll('td.pgbannercounterdesde div.acf-input div.acf-date-picker input.hasDatepicker');
apgbannercounterdesde[0].setAttribute('disabled','');

const apgbannercounterhasta=document.querySelectorAll('td.pgbannercounterhasta div.acf-input div.acf-date-picker input.hasDatepicker');
apgbannercounterhasta[0].setAttribute('disabled','');

const apgbannercounterhdesde=document.querySelectorAll('td.pgbannercounterhdesde div.acf-input div.acf-time-picker input.hasDatepicker');
apgbannercounterhdesde[0].setAttribute('disabled','');

const apgbannercounterhhasta=document.querySelectorAll('td.pgbannercounterhhasta div.acf-input div.acf-time-picker input.hasDatepicker');
apgbannercounterhhasta[0].setAttribute('disabled','');

}         
</script>

 <?php
 
  return $field;
}
//fin banner 2024



//codigo
//endoint para cargar info de hoteles
function go1mx_hoteles($data)
{

    $id = $data["id"];
    $campaignToken = $_GET["CampaignToken"];
    //$url = "https://pt-hotelquote-api.pricetravel.com/quote/list";
    //$url = 'https://apib2c.pricetravel.com/quote/list';
    $url= "https://apib2c.pricetravel.com/quote/internal/list";
    $inicio = "";
    $fin = "";
    switch (get_post_meta($id, "configuraciones_hotel_tipo_fecha_cotizacion", true))
    {
        case "1":
            $tipo_1_dias = get_post_meta($id, "configuraciones_hotel_tipo_1_dias", true);
            $tipo_1_estancia = get_post_meta($id, "configuraciones_hotel_tipo_1_estancia", true);
            $dias = intval($tipo_1_dias) + intval($tipo_1_estancia);
            $inicio = date('Y-m-d', strtotime(" +$tipo_1_dias days"));
            $fin = date('Y-m-d', strtotime(" +$dias days"));
            break;
        case "4": 
            $inicio = date("Y-m-d", strtotime(get_post_meta($id, "configuraciones_hotel_fecha_estatica_inicio", true)));
            $fin = date("Y-m-d", strtotime(get_post_meta($id, "configuraciones_hotel_fecha_estatica_fin", true)));
            break;
    }

    $hoteles = get_post_meta($id, "configuraciones_hotel_hoteles", true);
    $ids = [];
    if (is_array($hoteles)) {
        foreach ($hoteles as $hotel) {
            $info = json_decode($hotel);
            $ids[]= $info->code;
        }
    }
    $data = [
        "site"=>'www.pricetravel.com.mx',
        "source"=>'SPA-Hotel-List', 
        "ResponseTimeout"=> 3001,      
        "CampaignToken" => $campaignToken,
        "cache" => "false",
        "rooms" => "2",
        "checkIn" => $inicio,
        "checkOut" => $fin,
        "HotelIds" => implode(',',$ids)
    ];
    $query_url = $url.'?'.http_build_query($data);
    $result = wp_remote_get($query_url);
    $body = json_decode($result["body"]);
    if(is_object($body) && isset($body->rates)){
      return [
          "fechas"=>[
              "inicio"=>$inicio,
              "fin"=>$fin
          ],
          "orden" => intval(get_post_meta($id, "configuraciones_hotel_ordenamiento", true)),
          "ids" => $ids,
          "hoteles" => $body
      ];
    }
    return new WP_REST_Response(null, 400);
}

//endoint para cargar info de hoteles  "uri"=>"hoteldinamico",

function go1mx_hotel_detail($data)
{
  $id = $data["id"];
  $data = [
    "hotelId" => "".$id,
    "organizationId" => "1",
    "culture" => "es-mx"
/*    "imageProfileId" => "desktop",
    "query" => "{ query { gallery { cloudUri }, hotelID, location { city, country, state }, name, placeID , stars , title, uri, videos { thumbnailVideo url videoID } } }"
 */
  ];
  //$url = "https://api-search-hotel.pricetravel.com/api/hotel-content/";
  $url="https://apib2c.pricetravel.com/HotelFacade/HotelContent";
  $query_url = $url.'?'.http_build_query($data);
  $result = wp_remote_get($query_url);

  return json_decode($result["body"]);
}

/**
 * Register the /wp-json/acf/v3/posts endpoint so it will be cached.
 */
function wprc_add_acf_posts_endpoint( $allowed_endpoints ) {
  if ( ! isset( $allowed_endpoints[ 'go1mx/v1' ] ) || 
       ! in_array( 'hoteles', $allowed_endpoints[ 'go1mx/v1' ] ) ) {
      $allowed_endpoints[ 'go1mx/v1' ][] = 'hoteles';
  }
  return $allowed_endpoints;
}

add_filter( 'wp_rest_cache/allowed_endpoints', 'wprc_add_acf_posts_endpoint', 10, 1);


//filters
add_filter("acf/load_field/name=hoteles", "hoteles_load_field");
//tilters 20 12 2023
add_filter("acf/load_field/name=cargar_hoteles_dede_archivo_", "hotelesload_csv");
add_filter("acf/load_field/name=importar_hoteles_de_csv", "acfobtenreridarchivo");
add_filter('Myfiltergetidfilecsv','myget_fileid',10,1);
//filter 2024
add_filter('myGetIdImage','getUrlImagebanner',10,3);
//add_filter('acf/load_field/name=campaign_banner','getBannerCount');


//Hooks
add_action("acf/input/admin_enqueue_scripts", "my_admin_enqueue_scripts");
add_action("init", "go1mx_dynamic_landing_custom_post");
add_action("rest_api_init", function () {
  register_rest_route("go1mx/v1", "/hotel/(?P<id>\d+)", [
    "methods" => "GET",
    "callback" => "go1mx_hotel_detail",
  ]);
  register_rest_route("go1mx/v1", "/hoteles/(?P<id>\d+)", [
    "methods" => "GET",
    "callback" => "go1mx_hoteles",
  ]);
  register_rest_route("go1mx/v1", "/landings", [
    "methods" => "GET",
    "callback" => "go1mx_landings",
  ]);
  //go1mx_get_landings
  register_rest_route("go1mx/v1", "/list_landings", [
    "methods" => "GET",
    "callback" => "go1mx_get_landings",
  ]);
//go1mx_get_landings_header()
register_rest_route("go1mx/v1", "/header_home", [
  "methods" => "GET",
  "callback" => "go1mx_get_landings_header",
]);
register_rest_route("go1mx/v1", "/get_modifieddate", [
  "methods" => "GET",
  "callback" => "go1mx_get_landings_modifieddate",
]);

//go1mx_get_landings_updatemeta()
register_rest_route("go1mx/v1", "/update_metalandings", [
  "methods" => "POST",
  "callback" => "go1mx_get_landings_updatemeta",
]);

});