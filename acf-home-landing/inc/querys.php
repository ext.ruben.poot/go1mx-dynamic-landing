<?php
function getListLandingPages(){
    $args = array(
        'post_type'=>'dynamic-landing',
        'post_per_page'=>'100',
        'orderby'=>'title',
        'order'=>'ASC'
    );
    $query=new WP_Query($args);  
  

    while($query->have_posts()): $query->the_post();
    
      $landingpages= array();
      $landingpages[]=array(
            'title'=>get_the_title(),
            'description'=>get_the_content(),
            'link'=>get_permalink(),
            'image'=>get_the_post_thumbnail_url('full')
            );
  
      
	    
    endwhile; 
    echo json_encode($landingpages, JSON_FORCE_OBJECT);
    wp_reset_postdata();	
//wp_reset_query();

} //termina fucion
//getListLandingPages();

/*
  <template>
  <div class="autocomplete">
    <input
      type="text"
      @input="onChange"
      v-model="search"
      @keydown.down="onArrowDown"
      @keydown.up="onArrowUp"
      @keydown.enter="onEnter"
    />
    <ul
      id="autocomplete-results"
      v-show="isOpen"
      class="autocomplete-results"
    >
      <li
        class="loading"
        v-if="isLoading"
      >
        Loading results...
      </li>
      <li
        v-else
        v-for="(result, i) in results"
        :key="i"
        @click="setResult(result)"
        class="autocomplete-result"
        :class="{ 'is-active': i === arrowCounter }"
      >
        {{ result }}
      </li>
    </ul>
  </div>
</template>
 */