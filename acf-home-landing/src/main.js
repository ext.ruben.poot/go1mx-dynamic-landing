
import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel ';
import Dialog from 'primevue/dialog ';
import InputText from 'primevue/inputtext';3
import Textarea from 'primevue/textarea';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import AutoComplete from 'primevue/autocomplete';
import Calendar from 'primevue/calendar';
import Editor from 'primevue/editor';
import Checkbox from 'primevue/checkbox';
import RadioButton from 'primevue/radiobutton';
import Dropdown from 'primevue/dropdown';
//import FilterMatchMode from 'primevue/api'
import '@/assets/main.css';
import 'primevue/resources/themes/bootstrap4-light-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';



const app = createApp(App);
app.use(PrimeVue);

app.component('Button', Button);
app.component('TabView', TabView);
app.component('TabPanel', TabPanel);
app.component('Dialog', Dialog);
app.component('InputText', InputText);
app.component('Textarea', Textarea);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('AutoComplete', AutoComplete);
app.component('Calendar',Calendar);
app.component('Checkbox',Checkbox);
app.component('RadioButton',RadioButton);
app.component('Dropdown',Dropdown);
//app.component('FilterMatchMode',FilterMatchMode);
app.mount('#acf-home-dynamic');
//var Vue = require('vue');