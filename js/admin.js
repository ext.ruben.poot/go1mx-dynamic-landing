acf.add_filter('select2_args', function (args, $select, settings, field, instance) {

    if (field[0].dataset.name == 'hoteles') {
        args.tags = true;
        //args.tokenSeparators = [','];
        args.ajax.type = 'get';
        //args.ajax.url = 'https://search-places.pricetravel.com/suggest/hotel';
        args.ajax.url = 'https://search-algolia.pricetravel.com/Algolia/Hotel';
        args.ajax.minimumInputLength = 3;
        args.ajax.processResults = function (data) {
            //agregado el campo para label
            return {
                results: data.map(item => {
                    item.text = item.displayHtml;
                    item.name=item.text.split(',');
                    item.id = JSON.stringify({//guardar info de hoteles
                        'id': item.id,
                        'code': item.code,
                        'name': item.name[0]
                    });
                    return item;
                })
            };
        };
        args.ajax.data = function (params) {
            return {
                query: params.term,
                placeTypes: '14',
                language: 'es',
                from: 0,
                size: 10
            }
        };
        /*args.createTag = function (params) {
            var term = (params.term + "").trim();
        
            if (term === '') {
                return null;
            }
        
            return {
                id: term,
                text: term,
                newTag: true // add additional parameters
            }
        };*/
    }

    return args;

});